# Proyecto InverTecno

#### Objetivo

Se plantea realizar un invernadero en pequeña escala para representar el funcionamiento de un sistema meteorológico controlado por ordenador. El sistema se compone de factores naturales externos (sol, agua y viento) y equipamiento electrónico que controlado por software, hacen que el sistema responda según lo deseado.
Estos sistemas podrían ser instalado prácticamente en cualquier invernadero de frutas o verduras que requieran automatizar el control de temperatura, elevación de cortinas, etc. Puede continuar leyendo en Proyecto Invertecno.pdf

En la carpeta **Modelo prototipo/Partes v2** encontrarás las piezas básicas del prototipo y los conjuntos en el directorio 
**Ensamble prototipo** dónde encontrará todos los diversos ensambles.

En el directorio **Modelo prototipo/Recursos**, puede encontrar vídeo, imágenes y renders del proyecto.
