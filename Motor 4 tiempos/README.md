### Ejemplo ensamble motor realizado con A2Plus.
En FreeCAD debes abrir el archivo Conjunto Motor.FCStd

Todas las partes están ubicadas en el directorio Partes. Todavía faltan diseñar:
* Todo el sistema de arbol de levas....¿me ayudas a crear las piezas que faltan?

Para la animación del motor, creas una macro, pegar este código y luego ejecutar.


```
from a2p_solversystem import SolverSystem
import time
import FreeCADGui

class Motor():
	def __init__(self):
		self.angulo = 0
		self.solver = SolverSystem()

	def iniciar_animacion(self):
		for val in range(0, 90):
			self.angulo = val*4
			FreeCAD.ActiveDocument.b_Ciguenal_001_.Placement = App.Placement(App.Vector(0,0,0),App.Rotation(App.Vector(0,0,1),self.angulo))
			self.solver.solveSystem(App.ActiveDocument)
			FreeCADGui.updateGui()
			time.sleep(0.01)

	def stop(self):
		self.angulo = 0
		FreeCAD.ActiveDocument.b_Ciguenal_001_.Placement = App.Placement(App.Vector(0,0,0),App.Rotation(App.Vector(0,0,1),0))
		self.solver.solveSystem(App.ActiveDocument)

motor = Motor()
motor.iniciar_animacion()
```


